test('Aes Sanity', function() {
    var eclas = Utils.encryption;
    var e     = new eclas('mykey');
    assertObject(e);
    assertInstanceOf(eclas, e);
});

test('Aes Encription/Decription', function() {
    var eclas = Utils.encryption;
    var e     = new eclas('mykey');
    var str   = "testando123";
    assertEqual(e.decrypt(e.encrypt(str))                                , str, "Erro ao criptografar");
    assertEqual(e.decrypt("U2FsdGVkX1978mvKXpT4OIwuVCAi5NPzEVaLqBUgDuQ="), str, "Erro ao descriptografar");
});
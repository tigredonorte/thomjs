Kernel.registerExtension('DOM.table', {
                
    tableId: '',
    drawer: null,
    init: function(tableId){
        this.tableId = tableId;
        this.drawer = new this.Drawer();
    },

    draw: function(myList){
        this.drawer.buildHtmlTable(myList, this.tableId);
    },

    clear: function(){
        document.getElementById(this.tableId).innerHTML = "";
    },

    Drawer: function(){

        this.header     = [];
        this.htmlHeader = '';
        function init(header){
            if(header instanceof Array){
                this.header = header;
                return;
            }
            this.header = this.findHeader(header);
        }

        this.colLength = 0;
        function genRow(row){
            var row$ = $('<tr/>');
            for (var colIndex = 0,len = this.colLength ; colIndex <  len; colIndex++) {
                var cellValue = row[columns[colIndex]];
                if (cellValue === null) { cellValue = ""; }
                row$.append($('<td/>').html(cellValue));
            }
            return row$;
        }

        function buildHtmlTable(myList, elem) {
            this.colLength = columns.length;
            for (var i = 0 ; i < myList.length ; i++) {
                $(elem).append(this.append(myList[i]));
            }
        }

        function findHeader(myList){
            var header = [];
            var htmlHeader = $('<thead/><tr/>');
            for (var i = 0 ; i < myList.length ; i++) {
                var rowHash = myList[i];
                for (var key in rowHash) {
                    if ($.inArray(key, header) == -1){
                        header.push(key);
                        htmlHeader.append($('<th/>').html(key));
                    }
                }
            }
            this.htmlHeader = htmlHeader;
            return header;
        }
    }
});
// ---------------------------------------------------------------------------------------------
// NOTE: When your application gets this big you should break everything into separate files.
// ex. kernel-extension.js, main-hub.js, moduleA.js, moduleB.js, moduleC.js, moduleD.js, etc.
// ---------------------------------------------------------------------------------------------

// Define the main Hub - NOTE: 'main' is the default hub used if not otherwise specified in Kernel.start()
Kernel.hub.define('main', {

    ajax: Kernel.loadExt('ajax'),
    refreshStatusFeed: function() {
        this.ajax.setUrl('test/ajax.php/feed');
        this.ajax.send({newstatus: status}, function(ret, self){
            if(ret.getStatus()){
                self.broadcast('status-feed-update', ret.getResponse());
            }
            else {
                self.broadcast('error', "Couldn't fetch the feed: "+ret.getError());
            }
        }, this);
    },

    updateStatus: function(status) {
        this.ajax.setUrl('test/ajax.php/status');
        this.ajax.send({'newstatus': status}, function(ret, self){
            //alert(JSON.stringify(ret));
            if(ret.getStatus()){
                // Let everyone know that you updated your status
                self.broadcast('status-update', status);

                // Also update the feed
                self.refreshStatusFeed();
            }
            else {
                self.broadcast('error', "Couldn't update the status: "+ret.getError());
            }
        }, this);
    }

});

Kernel.module.define('StatusBox', {

    file: 'test/update-box.html',

    init: function() {

        var module = this;

        // Add handler to buttonB
        $('#update-button').click(function() {
            module.hub.updateStatus();
        });
    }

});

Kernel.module.define('StatusCount', {

    file: 'test/status-count.html',
    title: 'STATUS COUNT',
    portlet: true,

    init: function() {

        var self = this;

        // Listen for status-update messages and update count
        self.hub.listen('status-feed-update', function(data) {
            self.$moduleEl.find('.status-count').html(data.length);
        });
    }

});

Kernel.module.define('StatusList', {

    file: 'test/status-list.html',
    title: 'STATUS LIST',
    portlet: true,

    init: function() {

        var self = this;

        // Listen for status-update messages and update list
        self.hub.listen('status-feed-update', function(data) {
            // Clear the list
            self.$moduleEl.find('.status-list').children().remove();

            for (var i=0; i<data.length; i+=1) {
                self.$moduleEl.find('.status-list').append('<li>'+data[i]+'</li>');
            }

        });
    }

});


// Register the modules (id, type)
Kernel.register([
    {id: 'module-box', type: 'StatusBox', config: {renderTo: '#top'}},
    {id: 'module-count', type: 'StatusCount', config: {renderTo: '#center'}},
    {id: 'module-list', type: 'StatusList', config: {renderTo: '#center'}},
]);

// Start the modules after the DOM is ready
$('document').ready(function() {

    Kernel.start([
       {id: 'module-box'},
       {id: 'module-count'},
       {id: 'module-list'},
    ]);

});
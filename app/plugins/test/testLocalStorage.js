// ---------------------------------------------------------------------------------------------
// NOTE: When your application gets this big you should break everything into separate files.
// ex. kernel-extension.js, main-hub.js, moduleA.js, moduleB.js, moduleC.js, moduleD.js, etc.
// ---------------------------------------------------------------------------------------------

// Define the main Hub - NOTE: 'main' is the default hub used if not otherwise specified in Kernel.start()
Kernel.hub.define('main', {

    //ajax: Kernel.loadExt('ajax'),
    db: Kernel.loadExt('db'),
    refreshStatusFeed: function() {
        var self = this;
        this.db.init('feed');
        var results = this.db.list(2, 5, true);
        console.log('refreshStatusFeed ');
        self.broadcast('status-feed-update', results);
    },

    updateStatus: function(status) {
        var self = this;
        this.db.init('feed');
        var k = this.db.insert({'newstatus': status});
        if(k !== false){
            self.broadcast('status-update', status);
            self.refreshStatusFeed();
        }else{
            self.broadcast('error', "Couldn't update the status: ");
        }
    }

});

Kernel.module.define('StatusBox', {

    file: 'test/update-box.html',
    init: function() {

        var module = this;
        $('#update-button').click(function() {
            var v = $('#status-box').val();
            if(v === ""){alert("Digite um status válido!"); return;}
            module.hub.updateStatus($('#status-box').val());
        });
    }

});

Kernel.module.define('StatusCount', {

    file: 'test/status-count.html',
    title: 'STATUS COUNT',
    portlet: true,

    init: function() {

        var self = this;
        
        // Listen for status-update messages and update count
        self.hub.listen('status-feed-update', function(data) {
            //alert(JSON.stringify(data));
            console.log('StatusCount ' + JSON.stringify(data));
            self.$moduleEl.find('.status-count').html(Object.keys(data).length);
        });
        
        self.hub.refreshStatusFeed();
    }

});

Kernel.module.define('StatusList', {

    file: 'test/status-list.html',
    title: 'STATUS LIST',
    portlet: true,

    init: function() {

        var self = this;
        // Listen for status-update messages and update list
        self.hub.listen('status-feed-update', function(data) {
            //self.$moduleEl.find('.status-list').children().remove();
            var d;
            for(var i in data){
                d = data[i]['newstatus'];
                self.$moduleEl.find('.status-list').append('<li>'+d+'</li>');
            }

        });
        //self.hub.refreshStatusFeed();
    }

});


// Register the modules (id, type)
Kernel.register([
    {id: 'module-box', type: 'StatusBox', config: {renderTo: '#top'}},
    {id: 'module-count', type: 'StatusCount', config: {renderTo: '#center'}},
    {id: 'module-list', type: 'StatusList', config: {renderTo: '#bottom'}}
]);

// Start the modules after the DOM is ready
$('document').ready(function() {

    Kernel.start([
       {id: 'module-box'},
       {id: 'module-count'},
       {id: 'module-list'},
    ]);

});
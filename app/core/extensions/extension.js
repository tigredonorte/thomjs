Kernel.extend(Kernel, {
    extensions: {},
    loadExt: function(extName){
        try{
            if(typeof this.extensions[extName] === 'undefined'){return null;}
            this.extensions[extName].init();
            return this.extensions[extName];
        }catch(e){return null;}
    },
            
    registerExtension: function(extName, obj){
        if(typeof this.extensions[extName] !== 'undefined'){throw {'erro':'A extensão '+extName+' Já foi declarada'}}
        this.extensions[extName] = obj;
    }
});
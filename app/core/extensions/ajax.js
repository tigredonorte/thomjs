Kernel.registerExtension('ajax', {
    dataType: 'json',
    type    : 'POST',
    ret     : null,
    async   : true,
    urls    : '',
    isTest  : false,
    
    init: function(){
        this.ret = Kernel.loadExt('result');
        this.reset();
    },
            
    reset: function(){
        this.dataType = "json"; 
        this.type     = 'POST';
        this.async    = true;
        this.urls     = "";
        this.isTest   = false;
    },
            
    setDataType: function(type){
        this.dataType = type;
    },

    setUrl: function(type){
        this.urls = 'app/plugins/' + type;
    },

    setType: function(type){
        this.type = type;
    },

    setAssync: function(type){
        this.async = type;
    },

    getResponse: function(){
        return this.ret;
    },

    exCallback: function(callback, ret, context){
        try{
            if(typeof callback === 'function') {
                callback(ret, context);
            }
        }catch(e){console.log('core.ajax: ' + ext_lang.err_callback + JSON.stringify(e));}
    },

    send: function(data, callback, context){

        var r  = this.ret;
        var ajax = this;
        try{
            $.ajax({
                async: this.async,
                url: this.urls,
                type: this.type,
                data: data,
                dataType: this.dataType,
                success: function(json) {
                    try{
                        r.setResponse(json);
                        r.setStatus(true);
                        r.setError(ext_lang.op_comp_succ);
                        ajax.ret = r;
                        ajax.exCallback(callback, r, context);
                        return r;
                    }catch(e){
                        console.log('core.ajax: ' + ext_lang.err_op_ret + JSON.stringify(e));
                    }
                },

                error: function(erro){
                    try{
                        r.setError(ext_lang.err_com +erro['responseText']);
                        r.setStatus(false);
                        r.setResponse(erro);
                        ajax.r = r;
                        ajax.exCallback(callback, r, context);
                        return r;
                    }catch(e){
                        console.log('core.ajax: ' + ext_lang.err_error + e.message);
                    }
                }

            });

        }catch(e){
            try{
                r.setError(ext_lang.err_script + e.message); 
                r.setStatus(false);
                r.setException(e);
                ajax.ret = r;
                ajax.exCallback(callback, r);
                return r;
            }catch(f){
                console.log('core.ajax: '+ ext_lang.err_error +JSON.stringify(f));
            }
        }
    }
});
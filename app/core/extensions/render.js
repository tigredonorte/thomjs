// Extend Kernel to support ajax - we're using jQuery as the base library
Kernel.extend(Kernel, {

    pluginDir: "app/plugins/",

    render: function(instance) {
        if (instance.portlet) {
            this.renderPortlet(instance)
        }
        else {
            this.renderModule(instance)
        }
    },

    renderPortlet: function(instance) {
        // Wrap the module in a portlet
        $.get(this.pluginDir+instance.file, function(data) {
            $(instance.renderTo).prepend("<div class='portlet' id='"+instance.id+"'><div class='title'>"+instance.title+"</div>"+data+"</div>");

            // Save reference to module element for later
            instance.$moduleEl = $('#'+instance.id);

            // Make sure to finally call init()
            instance.init();
        });
    },

    renderModule: function(instance) {
        $.get(this.pluginDir+instance.file, function(data) {
            $(instance.renderTo).html(data);

            // Save reference to module element for later
            instance.$moduleEl = $(instance.renderTo);

            // Make sure to finally call init()
            instance.init();
        });
    },

    // Highjacking the startup gives you control over when module.init() gets called
    onStart: function(instance) {
        if (instance.file) {
            this.render(instance);
        }
        else {
            instance.init();
        }
    },

    onStop: function(instance) {

        // Allow module to handle its own shutdown first
        instance.kill();

        // Take care of any rendered elements
        var el = instance.$moduleEl;

        if (el) {
            el.fadeOut(500, function()
            {
                el.remove();
            });
        }
    }
});
ext_lang = {
    notbool:"val is not a boolean",
    undefined_var:"undefined var",
    lanched_exception: "Exception thowed",
    op_comp_succ: "Operation completed successfully!",
    err_op_ret : 'Execution error in callback function! ',
    err_com : 'Error communicating with the site <hr/> Details: ',
    err_error: 'Error executing error handling! ',
    err_script: 'Error executing script',
    err_callback:  'Exception thrown in callback'
};
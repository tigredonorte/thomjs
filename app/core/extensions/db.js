Kernel.registerExtension('db', {
    db: $.jStorage,
    table: '',
    instances:[],
    init: function(table){
        if(typeof table === 'undefined'){table = '';}
        if(typeof this.instances[table] === 'undefined'){
            this.instances[table] = new localStorageDB("library", localStorage);
        }
        this.table = table;
    },
    
    insert: function(value){
        var key = this.genKey();
        if($.jStorage.set(key, value) === false) return false;
        return key;
    },
            
    get: function(key){
        return $.jStorage.get(key);
    },
            
    list: function(page, limit, reverse){

        var total_pages, all, count;
        all   = $.jStorage.index();
        count = all.length;
        if(count < 1) return [];
        
        if(!is_bool(reverse)) reverse = false;
            
        limit       = (typeof limit !== 'undefined' && limit > 0) ? limit: count;
        total_pages = (limit > 0)?Math.ceil(count/limit): 1;
        if(typeof page === 'undefined' || page < 0) page = 0;
        if (page > total_pages) {page = total_pages;}
        if(limit < 1) limit = 10;
        
        var offset = limit*(page - 1);
        if(reverse === true){
            offset = count - limit - offset + 1;
        }
        if (offset < 0) offset = 0;
        limit = limit + offset;
        if(limit >= count) limit = count - 1;
        console.log("( pag:" + page + "; limit:" + limit + "; offset:" + offset + " totalpag:" + total_pages + " count:" + count + " )");
        
        var out = [],k;
        for(var i=offset; i< limit;i++){
            k = all[i];
            out.push($.jStorage.get(k));
        }
        return out;
    },      
            
    edit: function(key, value){
        return $.jStorage.set(key, value);
    },
    
    drop: function(key){
        return $.jStorage.deleteKey(key);
    },
            
    expires: function(key, time){
        return $.jStorage.setTTL(key, time);
    },
            
    clear: function(){
        $.jStorage.flush();
    },
    
    /**
     * @function genKey
     * @description Gera uma chave com alta probabilidade de ser única.
     */
    genKey: function(){
        return '__'+this.table + '_'+this.s4();
    },
    
    s4: function() {
        return Math.floor((1 + Math.random()) * 0x10000).toString(16);
    }
    
});
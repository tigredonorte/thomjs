Kernel.registerExtension('result', {

    response  : '',
    status    : true,
    error     : '',
    exception : '',

    init: function(){
        this.reset();
    },
            
    setResponse: function(val){
        this.response = val;
    },

    setStatus: function(val){
        if(!is_bool(val)) {
            throw {message: ext_lang.notbool, method:"result.setStatus"};
        }
        this.status = val;
    },

    setError: function(val){
        this.error = val;
    },

    setException: function(val){
        this.exception = val;
        this.status    = false;
        this.error     = this.exception.message;
        this.response  = "";
    },

    getResponse: function(v){
        if(typeof v != 'undefined'){
            try{
                if(typeof this.response[v] != 'undefined') {
                    return this.response[v];
                }
                return "";
            }catch(e){console.log(v);}
        }
        return this.response;
    },

    getStatus: function(){
        return this.status;
    },

    getError: function(){
        return this.error;
    },

    printError: function(){
        var e = this.getError();
        try{blockUI_error(e);} 
        catch(e){alert(e);}
    },

    reset: function(){
        this.response  = '';
        this.status    = true;
        this.error     = '';
        this.exception = '';
    }
});
Kernel.module.define('system/dialogs', {
    init: function() {
        // Listen events
        this.hub.listen('erro'   , function(msg) {this.displayMsg('erro'   , msg);});
        this.hub.listen('alert'  , function(msg) {this.displayMsg('alert'  , msg);});
        this.hub.listen('popup'  , function(msg) {this.displayMsg('popup'  , msg);});
        this.hub.listen('success', function(msg) {this.displayMsg('success', msg);});
    },
    
    displayMsg: function(target, v){
        try{
            var msg = '';
            if(typeof(v) == 'object' || typeof(v) == 'undefined'){
                if(typeof(v[target]) == 'undefined'){
                    console.log(ext_lang.undefined_var + ' msg.displayMsg');
                    return;
                } 
                msg = v[target];
            }else msg = v;
            $('#'+target).html(msg).fadeIn();
        }catch(e){
           console.log('msg.displayMsg: ', ext_lang.lanched_exception + ' - ' + JSON.stringify(v));
        }
    }

});